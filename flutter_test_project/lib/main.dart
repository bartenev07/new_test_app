import 'package:flutter/material.dart';
import 'package:flutter_test_project/src/common/router/app_routers.dart';


void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: AppRouter.splashScreen,
      routes: AppRouter.path
    );
  }
}