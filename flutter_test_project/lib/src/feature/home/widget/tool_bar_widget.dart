import 'package:flutter/material.dart';

class ToolBarWidget extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final List<Widget>? actions;
  const ToolBarWidget({super.key, required this.title, this.actions});

  @override
  Widget build(BuildContext context) {
    return AppBar(
        title: Text(title),
        titleTextStyle:
            TextStyle(color: Color.fromARGB(255, 96, 5, 93), fontSize: 25),
        backgroundColor: Color.fromARGB(255, 174, 18, 167),
        actions: actions,
      );
  }
  
  @override
  // TODO: implement preferredSize
  Size get preferredSize => const Size.fromHeight(50);
}