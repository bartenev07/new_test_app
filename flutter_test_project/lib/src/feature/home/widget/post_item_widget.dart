import 'package:flutter/material.dart';

class PostItem extends StatelessWidget {
  final String user;
  const PostItem({super.key, required this.user});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        children: [
          Row(
            children: [
              Image.asset(
                'assets/image/avatarok.jpg',
                width: 40,
                height: 40,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                user,
                style: TextStyle(color: Colors.white, fontSize: 20),
              )
            ],
          ),
          SizedBox(height: 10,),
          Image.asset('assets/image/post.jpeg'),
          SizedBox(height: 10,),
          Text('ksdkkdsfkvcdlslddlsfl ldfkaslkd alsdklska osikodaid oasdpiaosid paosidpoasid osdijkdjf ad', style: TextStyle(color: Colors.white),)
        ],
      ),
    );
  }
}