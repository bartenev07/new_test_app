import 'package:flutter/material.dart';
import 'package:flutter_test_project/src/feature/home/widget/post_item_widget.dart';
import 'package:flutter_test_project/src/feature/home/widget/tool_bar_widget.dart';


class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<String> arrUsers = [];
  @override
  Widget build(BuildContext context) {
    _generatorUsers();
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 42, 1, 75),
      appBar: ToolBarWidget(
        title: 'HomeScreen',
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 2),
            child: IconButton(
                onPressed: () {

                },
                icon: Icon(
                  Icons.location_on_outlined,
                  color: Color.fromARGB(255, 96, 5, 93),
                )),
          )
        ],
      ),
      body: ListView.separated(
        itemBuilder:(context, index) {
          return PostItem(user: arrUsers[index]);
        },
        itemCount: arrUsers.length,
        separatorBuilder: (context, index) {
         return SizedBox(height: 20,);
        },
      ),
    );
  }

  void _generatorUsers(){
    for (var i = 0; i < 100; i++) {
      arrUsers.add('User number: $i');
    }
  }
}
