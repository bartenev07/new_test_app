import 'package:flutter/material.dart';
import 'package:flutter_test_project/src/common/router/app_routers.dart';
import 'package:flutter_test_project/src/feature/home/widget/avater_widget.dart';
import 'package:flutter_test_project/src/feature/home/widget/tool_bar_widget.dart';

enum ProfileMenu{edit, logOut}
class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 42, 1, 75),
      appBar: ToolBarWidget(title: 'ProfileScreen',
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 2),
            child: PopupMenuButton<ProfileMenu>(
              iconColor:  Color.fromARGB(255, 96, 5, 93),
              onSelected: (value) {
                switch (value) {
                  case ProfileMenu.edit:
                    Navigator.of(context).pushNamed(AppRouter.editProf);
                  break;
                  case ProfileMenu.logOut:
                    print('lodout');
                  break;
                  default:
                }
              },
              itemBuilder: (context) {
                return [
                  PopupMenuItem(child: Text('Edit'), value: ProfileMenu.edit, ),
                  PopupMenuItem(child: Text('Log Out'), value:  ProfileMenu.logOut,)
                ];
              },
            ), 
          ),
        ],
      ),
      body: Column(
        children: [
          SizedBox(height: 20,),
          AvatarWidget(size: 80),
          SizedBox(height: 20,),
          const Text(
            'Pavel Bartenev',
            style: TextStyle(color: Colors.white, fontSize: 30),
          ),
          SizedBox(height: 10,),
          const Text(
            'Yoshkar-Ola',
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
          SizedBox(height: 15,),
          const Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                children: [
                  Text(
                  '472',
                  style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                  Text(
                  'Falowers',
                  style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
              Column(
                children: [
                  Text(
                  '820',
                  style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                  Text(
                  'Falowers',
                  style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
              Column(
                children: [
                  Text(
                  '472',
                  style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                  Text(
                  'Falowers',
                  style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ],
          ),
          Divider(height: 24,),
        ],
      ),
    );
  }
}