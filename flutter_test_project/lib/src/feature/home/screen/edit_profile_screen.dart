import 'package:flutter/material.dart';
import 'package:flutter_test_project/src/common/widgets/input_widget.dart';
import 'package:flutter_test_project/src/feature/home/widget/avater_widget.dart';
import 'package:flutter_test_project/src/feature/home/widget/tool_bar_widget.dart';

enum Gender { none, male, female, other }

class EditProfileScreen extends StatefulWidget {
  const EditProfileScreen({super.key});

  @override
  State<EditProfileScreen> createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  var gender = Gender.none;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 149, 124, 169),
      appBar: ToolBarWidget(title: 'Edit Profile'),
      body: LayoutBuilder(
        builder: (context, constraints) => SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraints.maxHeight),
            child: IntrinsicHeight(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    Stack(
                      children: [
                        AvatarWidget(size: 100),
                        Positioned(
                          child: Container(
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                            child: Icon(
                              Icons.edit,
                            ),
                          ),
                          bottom: 0,
                          right: 0,
                        )
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFieldWidget(
                      labelTextt: 'First name',
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFieldWidget(
                      labelTextt: 'last name',
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFieldWidget(
                      labelTextt: 'Phone number',
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFieldWidget(
                      labelTextt: 'Location',
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFieldWidget(
                      labelTextt: 'birthday',
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        border: Border.all(color: Colors.black, width: 2)
                      ),
                      child: Column(
                        children: [
                          Text('Gender'),
                          Row(
                            children: [
                              Expanded(
                                child: RadioListTile(
                                  contentPadding: EdgeInsets.zero,
                                  title: Text('Male'),
                                  value: Gender.male,
                                  groupValue: gender,
                                  onChanged: (value) {
                                    setState(() {
                                      gender = Gender.male;
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: RadioListTile(
                                  contentPadding: EdgeInsets.zero,
                                  title: Text('Female'),
                                  value: Gender.female,
                                  groupValue: gender,
                                  onChanged: (value) {
                                    setState(() {
                                      gender = Gender.female;
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: RadioListTile(
                                  contentPadding: EdgeInsets.zero,
                                  title: Text('Other'),
                                  value: Gender.other,
                                  groupValue: gender,
                                  onChanged: (value) {
                                    setState(() {
                                      gender = Gender.other;
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
