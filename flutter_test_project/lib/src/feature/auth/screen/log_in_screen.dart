import 'package:flutter/material.dart';
import 'package:flutter_test_project/src/common/router/app_routers.dart';
import 'package:flutter_test_project/src/common/widgets/background_widget.dart';
import 'package:flutter_test_project/src/common/widgets/input_widget.dart';
import 'package:flutter_test_project/src/feature/auth/widget/input_widgets.dart';

class LogInScreen extends StatefulWidget {
  const LogInScreen({super.key});

  @override
  State<LogInScreen> createState() => _LogInScreenState();
}

class _LogInScreenState extends State<LogInScreen> {
  final _fromkey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LayoutBuilder(
        builder: (context, constraints) => SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraints.maxHeight),
            child: IntrinsicHeight(
              child: BackgroundScreens(
                child: Form(
                  key: _fromkey,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 24),
                    child: Column(
                      children: [
                        Spacer(),
                        const Icon(
                          Icons.hourglass_empty,
                          size: 100,
                        ),
                        const Text(
                          'Log In',
                          style: TextStyle(color: Colors.black, fontSize: 30,),
                        ),
                        const SizedBox(height: 70,),
                        TextFieldWidget(labelTextt: 'Email',),
                        const SizedBox(height: 10,),
                        InputPasswordWidget(),
                        const SizedBox(height: 15),
                        SizedBox(
                          height: 35,
                          child: TextButton(
                            onPressed: (){
                          
                            }, 
                            style: TextButton.styleFrom(foregroundColor: Colors.black),
                            child: Text('Forgot password?',),
                          ),
                        ),
                        SizedBox(
                          height: 35,
                          child: TextButton(
                            onPressed: (){
                          
                            }, 
                            style: TextButton.styleFrom(foregroundColor: Colors.black),
                            child: Text('Already have an account?',),
                          ),
                        ),
                        const SizedBox(height: 30),
                        SizedBox(
                          width: 100,
                          child: ElevatedButton(onPressed: (){
                            _fromkey.currentState!.validate();
                            Navigator.of(context).pushReplacementNamed(AppRouter.main);
                          }, style: ElevatedButton.styleFrom(foregroundColor: Colors.pink.withOpacity(0.7), backgroundColor: Colors.black),
                           child: Text('Login', style: TextStyle(fontSize: 17),),),
                        ),
                        const Spacer(),
                      ],
                    ),
                  ),
                ),
                ),
              ),
            ),
          ),
        ),
      ); 
  }
}