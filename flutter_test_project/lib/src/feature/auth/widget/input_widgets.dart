import 'package:flutter/material.dart';
import 'package:flutter_test_project/src/common/utils/validators.dart';


class InputPasswordWidget extends StatefulWidget {

  const InputPasswordWidget({super.key});

  @override
  State<InputPasswordWidget> createState() => _InputPasswordWidgetState();
}

class _InputPasswordWidgetState extends State<InputPasswordWidget> {
  var _isObscureText = true;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      cursorColor: Colors.black,
      obscureText: _isObscureText,
      decoration: InputDecoration(
        labelText: 'Password',
        suffixIcon: IconButton(
          color: Colors.black,
          onPressed: () {
            setState(() {
              _isObscureText = !_isObscureText;
            });
          },
          icon: _isObscureText
              ? Icon(Icons.visibility_off)
              : Icon(Icons.visibility),
        ),
        contentPadding: EdgeInsets.symmetric(horizontal: 20),
        labelStyle: const TextStyle(color: Colors.black, fontSize: 20),
        focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            borderSide: BorderSide(color: Colors.black, width: 2)),
        border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            borderSide: BorderSide(color: Colors.black, width: 2)),
      ),
      validator: (password) => Validator().password(password),
    );
  }
}
