import 'package:flutter/material.dart';

class TextFieldWidget extends StatelessWidget {
  final  String labelTextt;
  const TextFieldWidget({super.key, required this.labelTextt});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      cursorColor: Colors.black,
      decoration: InputDecoration(
        labelText: labelTextt,
        contentPadding: EdgeInsets.symmetric(horizontal: 20),
        labelStyle: TextStyle(color: Colors.black, fontSize: 20),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            borderSide: BorderSide(color: Colors.black, width: 2)),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        enabledBorder:  OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            borderSide: BorderSide(color: Colors.black, width: 2)),  
      ),
    );
  }
}