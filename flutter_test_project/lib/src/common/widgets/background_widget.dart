import 'package:flutter/material.dart';

class BackgroundScreens extends StatelessWidget {
  final Widget child;
  const BackgroundScreens({super.key, required this.child});
  
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: [Colors.yellow, Colors.pink.withOpacity(0.7)],
            begin: Alignment.topRight,
            end: Alignment.bottomLeft),
      ),
      child: child,
    );
  }
}
