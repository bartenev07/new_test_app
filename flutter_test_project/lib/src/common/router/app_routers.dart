import 'package:flutter_test_project/src/feature/auth/screen/log_in_screen.dart';
import 'package:flutter_test_project/src/feature/home/screen/edit_profile_screen.dart';
import 'package:flutter_test_project/src/feature/home/screen/home_screen.dart';
import 'package:flutter_test_project/src/feature/home/widget/bottomNavigation.dart';
import 'package:flutter_test_project/src/feature/loading_screen/splash_sreen.dart';

class AppRouter {
  static final path = {
    '/': (context) => const SplashScreen(),
    '/logIn': (context) => const LogInScreen(),
    '/home': (context) => const HomeScreen(),
    '/main': (context) => const BottonNavigation(),
    '/editProf': (context) => const EditProfileScreen(),
  };

  static const splashScreen = '/';
  static const logIn = '/logIn';
  static const home = '/home';
  static const main = '/main';
  static const editProf = '/editProf';
}
